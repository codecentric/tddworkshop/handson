package nl.codecentric.tdd.handson;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class MineSweeperTest {

    private MineSweeper mineSweeper;

    @BeforeEach
    void setUp() {
        mineSweeper = new MineSweeper();
    }

    @Test
    void green() {
        assertThat(mineSweeper).isNotNull();
    }

    @Test
    void red() {
        assertThat(mineSweeper).isNull();
    }
}
